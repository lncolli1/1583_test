public class GeneratePrimes{
    public static void main(String[] args){
        int upperLimit = 10000000;
        int numPrimesFound = 0;
        
        for(int potentialPrime = 2; potentialPrime <= upperLimit; potentialPrime++){
            boolean isPrime = true;
            
            for(int potentialDivisor = 2; potentialDivisor <= Math.sqrt(potentialPrime) / 2; potentialDivisor++){
                if(potentialPrime % potentialDivisor == 0){
                    isPrime = false;
                    break;
                }
            }
            
            if(isPrime){
                numPrimesFound++;
                System.out.println(potentialPrime + "\t");
                if(numPrimesFound % 10 == 0){
                    System.out.println();
                }
            }
        }
        System.out.println("\nNumber of primes below" + upperLimit +": " + numPrimesFound);
    }
}