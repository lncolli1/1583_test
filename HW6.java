public class Balcony extends Room {
  private String message = "You are on the Balcony, go South to the North Hall";

    public String getMessage() {
      return this.message;
    }
    public String getRoomName() {
      return "Balcony";
    } 
    
}

public class Bedroom1 extends Room {
  private String message = "You are in BedRoom 1, You can go South to BedRoom2 or East to North Hall";

    
    public String getMessage() {
      return this.message;
    }    
    
    public String getRoomName() {
      return "Bedroom1";
    }     
}

public class Bedroom2 extends Room {
  private String message = "You are in BedRoom 2, you can go North to BedRoom 1 or East to South Hall";

    
    public String getMessage() {
      return this.message;
    }    
    
    public String getRoomName() {
      return "Bedroom2";
    }     
}

public class DiningRoom extends Room {
  private String message = "You are in the Dining Room, you can go West to South Hall or North to the Kitchen";

    
    public String getMessage() {
      return message;
    }
    
    public String getRoomName() {
      return "DiningRoom";
    }     
}
import java.util.Scanner;
import java.util.Random;


public class Dungeon {

    private  static char MOVES[] = new char[] {'n', 's', 'e', 'w'};
    
    private Room bedroom1 = new Bedroom1();
    private Room bedroom2 = new Bedroom2();
    private Room balcony = new Balcony();
    private Room diningRoom = new DiningRoom();
    private Room northHall = new NorthHall();
    private Room southHall = new SouthHall();
    private Room kitchen = new Kitchen();

    private Monster mike = null;
    private Hero sully = null;
    
    private Random random = new Random();
    
    public Dungeon() {
      bedroom1.setSouth(bedroom2);
      bedroom1.setEast(northHall);
      
      bedroom2.setNorth(bedroom1);
      bedroom2.setEast(southHall);
      
      diningRoom.setWest(southHall);
      diningRoom.setNorth(kitchen);
      
      kitchen.setWest(northHall);
      kitchen.setSouth(diningRoom);
      
      southHall.setWest(bedroom2);
      southHall.setNorth(northHall);
      southHall.setEast(diningRoom);
      
      northHall.setEast(kitchen);
      northHall.setSouth(southHall);
      northHall.setWest(bedroom1);
      northHall.setNorth(balcony);
      
      balcony.setSouth(northHall);
   }

    private void initialize() {
        mike = new Monster("Mike", 100, 10, 0);
        sully = new Hero("Sully", 200, 20, 17);
        mike.setCurrentRoom(bedroom1);
        sully.setCurrentRoom(balcony);
        printPlayers();
    }

    public void resetTheHero() {
       System.out.println("GAME OVER!!!!!!");
       System.exit(0);
    }
    
    public void resetTheMonster() {
        mike = new Monster("Mike", 100, 10, 0);
        sully = new Hero("Sully", 200, 20, 17);
        mike.setCurrentRoom(bedroom1);
        sully.setCurrentRoom(balcony);
        printPlayers();
    }    
    
    private void printPlayers() {
        System.out.println(mike);
        System.out.println(sully);
    }
    
    private boolean playersSameRoom() {
        return mike.getCurrentRoom().getRoomName().equals(sully.getCurrentRoom().getRoomName());
    }
    
    private void performCombat() {
        RunCombat combat = new RunCombat();
        combat.doCombat(this, mike, sully);
    }
    
   public static void main(String[] args) {
       Dungeon dungeon = new Dungeon();
 
        dungeon.initialize();
        
         do {
            dungeon.moveMonster();
            dungeon.moveHero();
            dungeon.printPlayers();
            
            if ( dungeon.playersSameRoom() ) {
                System.out.println("WE FIGHT!!!!!");
                dungeon.performCombat();
            }
        } while (true);
   }
   
   public void moveMonster() {
       int rand = Math.abs(random.nextInt() % 4);;
        move(mike, MOVES[rand]);
   }
   
   public void moveHero() {
      Scanner input = new Scanner(System.in);
      // this is to learn what not to do. never put true
      
      boolean moved = false;
      char move;
      do {

            System.out.println();
            System.out.println(sully.getCurrentRoom().getMessage());

            System.out.println("Enter n - north, s - south, e - east, w - west, q - quit ");
            System.out.printf("Enter your Move : %n");
 
            move = input.next().toLowerCase().charAt(0);
            
            moved = move(sully, move);

      } while(!moved);
   }

    public boolean move(GameCharacter gc, char move)
    {
        Room currentRoom = gc.getCurrentRoom();
        Room newRoom = null;

        switch(move) {
            case 'n' : if (currentRoom.goNorth() != null)
                          newRoom = currentRoom.goNorth();
                       else
                            System.out.println("This move is illegal");
                    break;

            case 'e' :  if (currentRoom.goEast() != null)
                            newRoom = currentRoom.goEast();
                       else
                            System.out.println("This move is illegal");
                    break;

            case 'w' : if (currentRoom.goWest() != null)
                          newRoom = currentRoom.goWest();
                       else
                            System.out.println("This move is illegal");
                    break;

            case 's' :  if (currentRoom.goSouth() != null)
                          newRoom = currentRoom.goSouth();
                       else
                            System.out.println("This move is illegal");
                    break;

            default: break;
        }
        
        if (newRoom != null)
        {
            gc.setCurrentRoom(newRoom);
            return true;
        }

        return false;
   }
   
}
import java.util.Random;

public abstract class GameCharacter{

    private Room currentRoom = null;

    private String name;

    private int health;

    private int attackDamage;
    
    private int manaPower;

    private Random gen;

    

    public GameCharacter(String name, int health, int attackDamage, int manaPower){

        this.name = name;

        this.health = health;
        
        this.manaPower = manaPower;

        this.attackDamage = attackDamage;

        gen = new Random();


    }

    public Room getCurrentRoom() {
        return  this.currentRoom;
    }
    
    public void setCurrentRoom(Room room) {
        this.currentRoom = room;
    }

    public void takeDamage() {

        this.health = this.health - this.attackDamage;

    }
    

    public void setHealth(int health) {
        this.health = health;

    }


    

    public void increaseHealth(int points){

        this.health += points;

    }

    

    public void attack(GameCharacter gc){

        int damage = this.attackDamage + gen.nextInt(10) + 1;

        gc.setHealth(damage);
    }

    

    public abstract void takeTurn(GameCharacter gc);

    

    public String getName(){

        return this.name;

    }

    

    public int getHealth(){

        return this.health;

    }

    

    public int getAttackDamage(){

        return this.attackDamage;

    }

    

    public int getManaPower(){

        return this.manaPower;

    }
    
    
    public void chargeMana(){

        this.manaPower += 3;

    }

    public boolean getIsRunningAway(){

        return false;

    }
    
    public String toString(){

        String str = "Name: " + getName() + "\n";

        str += "In Room: " + getCurrentRoom().getRoomName() + "\n";
        
        str += "Health: " + getHealth() + "\n";

        str += "AttackDamage: " + getAttackDamage() + "\n";

        str += "ManaPower: " + getManaPower() + "\n";

        return str; 

    }

}
import java.util.Scanner;

public class Hero extends GameCharacter {

    private boolean isRunningAway;

    

    public Hero(String name, int health, int attackDamage, int manaPower){

        super(name, health, attackDamage, manaPower);

        this.isRunningAway = false;

    }

    

    public void castSpell(GameCharacter gc){

        int damage; 

        if(getManaPower() > 3){

            damage = gc.getHealth() / 2;

            gc.setHealth(damage);

            

        } else{

            System.out.println("Not enough mana!");


        }

    }
    
    public boolean getIsRunningAway(){

        return this.isRunningAway;

    }

    

    public void takeTurn(GameCharacter gc){

        

    }

}

public class Kitchen extends Room {
  private String message = "You are in the kitchen, you can go either West to North Hall or South to the Dining Room";

    public String getMessage() {
      return this.message;
    }
    
    public String getRoomName() {
      return "Kitchen";
    }     
}
public class Monster extends GameCharacter{

    

    public Monster(String name, int health, int attackDamage, int manaPower){

        super(name, health, attackDamage, manaPower);

    }

    

    public void takeTurn(GameCharacter gc){

        attack(gc);

    }

}

public class NorthHall extends Room {
  private String message = "You are in North Hall, you can go either North to the Balcony, West to Bedroom 1, South to South Hall, or East to the Kitchen";
    public String getMessage() {
      return this.message;
    }
    public String getRoomName() {
      return "NorthHall";
    } 
}
public class Room {
  private Room north = null;
  private Room south = null;
  private Room east = null;
  private Room west = null;  


    public String getMessage() {
      return null;
    }
    
    public void setNorth(Room north) {
      this.north = north;
    }
    
    public void setSouth(Room south) {
      this.south = south;
    }
    
    public void setEast(Room east) {
      this.east = east;
    }
    
    public void setWest(Room west) {
      this.west = west;
    }    
    
    public Room goNorth() {
      return this.north;
    }
    
    public Room goSouth() {
      return this.south;
    }
    
    public Room goEast() {
      return this.east;
    }
    
    public Room goWest() {
      return this.west;
    }  
    
    public String getRoomName() {
      return "No Room Name";
    } 
}
import java.util.Scanner;

public class RunCombat {
     
    public void doCombat(Dungeon dungeon, Monster monster, Hero hero) {
        Scanner input = new Scanner(System.in);


            int option;
            boolean monsterAlive = true;
            boolean heroAlive = true;
            do { 

                System.out.println();
    
                System.out.println("Enter 1 - Sword Attack, 2 - Cast a Spell, 3 - Charge Mana, 4 - Flee ");
                System.out.printf("Enter your Move : %n");
    
                option = input.nextInt(); 
                
                switch(option) {
                    case 1 : monster.takeDamage();
                             hero.takeDamage();
                            break;
    
                    case 2 : hero.castSpell(monster);
                             hero.takeDamage();                
                            break;
    
                    case 3 : hero.chargeMana();
                             hero.takeDamage();                
                            break;
    
                    case 4 : hero.getIsRunningAway();
    
    				default : System.out.println("Invalid option. Please Try Again");
                }
    
                System.out.println(monster);
                System.out.println(hero);
                
                if (hero.getHealth() <= 0) {
                    heroAlive = false;
                    System.out.println("Hero Is Dead!");
                }
                
                if (monster.getHealth() <= 0) {
                    monsterAlive = false;
                    System.out.println("Monster Is Dead!");
                }
            } while (option != 4 && monsterAlive && heroAlive );
            
            if ( ! heroAlive ) {
                dungeon.resetTheHero();
            }
            
            if ( ! monsterAlive ) {
                dungeon.resetTheMonster();
            }
    }
}

public class SouthHall extends Room {
  private String message = "You are in the South Hall, You can go either West to BedRoom 1, North to North Hall, or East to Dining Room";
    public String getMessage() {
      return this.message;
    }
    
    public String getRoomName() {
      return "SouthHall";
    }     
}
