/*
 * Author: Luke Collins 
 * Program: Combat Calculator
 * Date: 2 March 2016
*/
import java.util.Scanner;
import java.util.Random;

public class CombatCalculatorExpansion{

    public static void main(String[] args){
    
        int monstersHealth = 100;
        int monsterAttackpower = 0;
        String monstersName;
        int xp;
        int yourHealth = 0;
        int yourAttackpower = 0;
        int magicPower = 0;
        int magicPoints = 0;
        int statPoints = 20;
        
        boolean loopControl = true;
            while (loopControl){
                
        
        System.out.println("***************************");
        System.out.println("Health: " + yourHealth);
        System.out.println("Attack: " + yourAttackpower);
        System.out.println("Magic: " + magicPower);
        System.out.println("***************************");
        System.out.println("5.) +10 Health");
        System.out.println("6.) +1 Attack");
        System.out.println("7.) +3 Magic");
        System.out.println("You have 20 points to spend: " + statPoints);
        System.out.println("***************************");
        
        int choice;
        Scanner inputReader = new Scanner(System.in);
        choice = inputReader.nextInt();
        
        if(choice == 5){
            yourHealth = yourHealth + 10;
            statPoints = statPoints - 1;
        } else if(choice == 6){
            yourAttackpower = yourAttackpower + 1;
            statPoints = statPoints - 1;
        } else if(choice == 7){
            magicPower = magicPower + 3;
            statPoints = statPoints - 1;
        } else {
            System.out.println("I don't understand that command.");
        } if(statPoints <= 0){
            loopControl = false;
            System.out.println("You don't have any stat points left to spend.");
        }
        System.out.println("Combat Options");
        
        System.out.println("1.) Sword Attack");
        System.out.println("2.) Cast Spell");
        System.out.println("3.) Charge Mana");
        System.out.println("4.) Run Away");
        System.out.println("What Action do you want to perform?");

        
        if(choice == 1){
            monstersHealth = monstersHealth - yourAttackpower;
            System.out.println("You strike the goblin with your sword for 12 damage!");
            System.out.println(monstersHealth + " health remaining");
        } else if (choice == 2){
            if(magicPower >= 3){ 
            System.out.println("You cast the weaken spell on the monster.");
            monstersHealth = monstersHealth / 2;
            } else
            System.out.println("You don't have enough mana.");
            System.out.println(monstersHealth + " health remaining");
        } else if (choice == 3){
            magicPower = magicPower + 1;
            System.out.println("You focus and charge your magic power.");
            
        } else if (choice == 4){
            
            loopControl = false;
            System.out.println("You run away!");
        } else if (choice >= 8){
            System.out.println("I don't understand that command.");
        }
        if(monstersHealth <= 0){
           loopControl = false;
           System.out.println("You defeated the goblin!");
        }
        
    
    Random randomGenerator = new Random();
    int randomNumber = randomGenerator.nextInt(2);
    if (randomNumber == 0){
        monstersName = "Goblin";
        monstersHealth = 75 + randomGenerator.nextInt(24);
        monsterAttackpower = 8 + randomGenerator.nextInt(4);
        xp = 1;
    }
    else if (randomNumber == 1){
        monstersName = "Orc";
        monstersHealth = 100 + randomGenerator.nextInt(24);
        monsterAttackpower = 12 + randomGenerator.nextInt(4);
        xp = 3;
    }
    else if (randomNumber == 2){
        monstersName = "Troll";
        monstersHealth = 150 + randomGenerator.nextInt(49);
        monsterAttackpower = 15 + randomGenerator.nextInt(4);
        xp = 5;
        
            }
        }
    }
}