import java.util.Scanner;

public class AdventureGame{
    
       final static int NORTH = 0;
       final static int EAST = 1;
       final static int WEST = 2;
       final static int SOUTH = 3;
       
       static String [] description = new String[7];
       static int[][] exit = new int[7][4];

    public static void init(){
          
        description [0] = "Bedroom 2";
        description [1] = "South Hall";
        description [2] = "Dining Hall";
        description [3] = "Bedroom 1";
        description [4] = "Norht Hall";
        description [5] = "Kitchen";
        description [6] = "Balcony";

        
        exit [0] [NORTH] = 3;
        exit [0] [EAST] = 1;
        exit [0] [WEST] = -1;
        exit [0] [SOUTH] = -1;
        
        exit [1] [NORTH] = 4;
        exit [1] [EAST] = 2;
        exit [1] [WEST] = 0;
        exit [1] [SOUTH] = -1;
        
        exit [2] [NORTH] = 5;
        exit [2] [EAST] = -1;
        exit [2] [WEST] = 1;
        exit [2] [SOUTH] = -1;
        
        exit [3] [NORTH] = -1;
        exit [3] [EAST] = 4;
        exit [3] [WEST] = -1;
        exit [3] [SOUTH] = 0;
        
        exit [4] [NORTH] = 6;
        exit [4] [EAST] = 5;
        exit [4] [WEST] = 3;
        exit [4] [SOUTH] = 1;
        
        exit [5] [NORTH] = -1;
        exit [5] [EAST] = -1;
        exit [5] [WEST] = 4;
        exit [5] [SOUTH] = 2;
        
        exit [6] [NORTH] = -1;
        exit [6] [EAST] = -1;
        exit [6] [WEST] = -1;
        exit [6] [SOUTH] = 4;
      }
    
    public static void main(String[] args){
       init();
       
      
    Scanner input = new Scanner(System.in);
    
    int current = 0;
    
    String choice = "";
    
        while(choice.compareToIgnoreCase("q") != 0){
            System.out.println(description[current]);
            System.out.println("Type n to go North. Type s to go South. Type w to go West. Type e to go East.");
            choice = input.nextLine();
        if(choice.compareToIgnoreCase("n") == 0){
            if(exit [current][NORTH] != -1){
                current = exit [current][NORTH];
            } else {
                System.out.println("There's no room in that direction.");
            }
        }
        if(choice.compareToIgnoreCase("e") == 0){
            if(exit [current][EAST] != -1){
                current = exit [current][EAST];
            } else {
                System.out.println("There's no room in that direction.");
            }
        }
        if(choice.compareToIgnoreCase("w") == 0){
            if(exit [current][WEST] != -1){
                current = exit [current][WEST];
            } else {
                System.out.println("There's no room in that direction.");
            }
        }
        if(choice.compareToIgnoreCase("s") == 0){
            if(exit [current][SOUTH] != -1){
                current = exit [current][SOUTH];
            } else {
                System.out.println("There's no room in that direction.");
            }
    }
        
}
}
}