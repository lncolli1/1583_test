/*
 * Author: Luke Collins 
 * Program: Combat Calculator
 * Date: 3 February 2016
*/
import java.util.Scanner;

public class CombatCalculator6{

    public static void main(String[] args){
    
        int goblinsHealth = 100;
        int goblinAttackpower = 15;
        
        int yourHealth = 100;
        int yourAttackpower = 12;
        int herosPower = 0;
        
        boolean loopControl = true;
            while (loopControl){
        
        System.out.println("You are fighting a goblin!");
        System.out.println("The monster HP: 100");
        System.out.println("Your HP: 100");
        System.out.println("Your MP: 0");
        
        System.out.println("Combat Options");
        
        System.out.println("1.) Sword Attack");
        System.out.println("2.) Cast Spell");
        System.out.println("3.) Charge Mana");
        System.out.println("4.) Run Away");
        System.out.println("What Action do you want to perform?");
        
        int choice;
        Scanner inputReader = new Scanner(System.in);
        choice = inputReader.nextInt();
        
        
        if(choice == 1){
            goblinsHealth = goblinsHealth - yourAttackpower;
            System.out.println("You strike the goblin with your sword for 12 damage!");
            System.out.println(goblinsHealth + " health remaining");
        } else if (choice == 2){
            if(herosPower >= 3){ 
            System.out.println("You cast the weaken spell on the monster.");
            goblinsHealth = goblinsHealth / 2;
            } else
            System.out.println("You don't have enough mana.");
            System.out.println(goblinsHealth + " health remaining");
        } else if (choice == 3){
            herosPower = herosPower + 1;
            System.out.println("You focus and charge your magic power.");
            
        } else if (choice == 4){
            
            loopControl = false;
            System.out.println("You run away!");
        } else {
            System.out.println("I don't understand that command.");
        }
        if(goblinsHealth <= 0){
           loopControl = false;
           System.out.println("You defeated the goblin!");
        }
        
    }    
}
}